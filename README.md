# AlexaDevCamp

Hallo,
schön, dass Du beim Workshop dabei bist. Damit wir zum DevCamp sofort loslegen können, bitte ich dich die folgende Installationsanweisungen durchzuführen:

## Amazon Developer-Account erstellen
Um einen eigenen Skill erstellen zu können ist ein Account bei developer.amazon.com erforderlich: https://developer.amazon.com/de/

## Installation ngrok
Da wir ein eigenes Backend erstellen, auf das Alexa dann zugreifen soll, müssen wir dieses vom Internet aus erreichbar machen.
ngrok hilft dabei einen Tunnel zu einem lokalen Server herzustellen. 
Zur Vorbereitung genügt es ngrok zu installieren (kostenloser Useraccount notwendig):
https://ngrok.com/

## Installation eines JDK
Ich habe für das Projekt JDK 8 verwendet. Sicherlich funktionieren die neueren aber genauso gut.
https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

## Git-Client installieren (sofern noch nicht vorhanden)
git herunterladen und installieren: https://git-scm.com/downloads

## Maven installieren
maven herunterladen und installieren: https://maven.apache.org/download.cgi

## Git-Sourcen auschecken
```
git clone https://gitlab.com/koernerm/alexadevcamp.git
```

## Projektabhängigkeiten herunterladen
Ins Verzeichnis wechseln, in dem die Sourcen heruntergeladen wurden (in diesem Verzeichnis befindet sich eine pom.xml):
```
cd alexadevcamp
```

Folgenden Befehl ausführen:
```
mvn install
```

Wenn alles geklappt hat, wird in der Kommandozeile folgendes ausgegeben:
```
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
```


## IDE installieren
Um zum devcamp richtig mitzumachen benötigt ihr natürlich auch eine Entwicklungsumgebung. Hier könnt ihr verwenden, womit ihr euch auskennt.
Zum Beispiel Spring Tools for Eclipse:
https://spring.io/tools


Solltet Ihr Probleme bei der Installation haben, würde ich mich sehr über eine Mail an michael.koerner@msg.group freuen.


