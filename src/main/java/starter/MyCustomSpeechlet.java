package starter;

import java.nio.channels.SeekableByteChannel;

import org.springframework.stereotype.Service;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.SpeechletException;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.OutputSpeech;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SimpleCard;

@Service
public class MyCustomSpeechlet implements Speechlet {

    @Override
    public void onSessionStarted(SessionStartedRequest request, Session session) throws SpeechletException {    
    }

	@Override
    public SpeechletResponse onLaunch(LaunchRequest request, Session session) throws SpeechletException {
    	PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
    	outputSpeech.setText("in OnLaunch");
		SpeechletResponse newTellResponse = SpeechletResponse.newTellResponse(outputSpeech);
		return newTellResponse;
    }

    @Override
    public SpeechletResponse onIntent(IntentRequest request, Session session) throws SpeechletException {    
    	PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
    	outputSpeech.setText("in OnInit");
		SpeechletResponse newTellResponse = SpeechletResponse.newTellResponse(outputSpeech);
		return newTellResponse;
    }	

    @Override
    public void onSessionEnded(SessionEndedRequest request, Session session) throws SpeechletException {

    }
}